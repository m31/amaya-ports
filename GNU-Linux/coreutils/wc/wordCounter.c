#define isDelim(x) (x == ' ' || x == '\n')
#define isToken(x) (x != ' ' && x != '\n')

unsigned int sw = 0;
unsigned char w_readTokens = 1, w_readDelimiters = 1;

void wordCounter(char c)
{
	if(c == -1) return;

	if(isToken(c) && w_readTokens)
	{
		sw++;
		w_readTokens = 0;
		w_readDelimiters = 1;
	}

	if(isDelim(c) && w_readDelimiters)
	{
		w_readDelimiters = 0;
		w_readTokens = 1;
	}
}

void resetWordCounter()
{
	sw = 0;
	w_readTokens = 1;
	w_readDelimiters = 1;
}
