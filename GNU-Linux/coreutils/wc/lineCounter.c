unsigned int sl = 0;
char lineCounter_old = 0;

void lineCounter(char c)
{
	if((c == '\n') || (c == -1 && lineCounter_old != '\n')) sl++;
	lineCounter_old = c;
}

void resetLineCounter()
{
	sl = 0;
	lineCounter_old = 0;
}
