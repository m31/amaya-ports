#include <stdio.h>
#include "lineCounter.h"
#include "wordCounter.h"
#include "charCounter.h"
#include "options.h"

void printInfo(char *path, unsigned char options)
{
	FILE *f;
	if(path) f = fopen(path, "r");
	else f = stdin;
	if(!f) return;

	char c;

	do
	{
		c = fgetc(f);

		if(options & OPT_L) lineCounter(c);
		if(options & OPT_W) wordCounter(c);
		if(options & OPT_C) charCounter(c);

	} while(c != EOF);

	if(options & OPT_L) printf("%d ", sl);
	if(options & OPT_W) printf("%d ", sw);
	if(options & OPT_C) printf("%d ", sc);
	if(path) printf("%s ", path);
	fputc('\n', stdout);

	resetLineCounter();
	resetWordCounter();
	resetCharCounter();

	fclose(f);
}
