#include <string.h>
#include "printInfo.h"
#include "options.h"

int main(int argc, char *argv[])
{
	unsigned char options = 0, use_stdin = 1; int i;

	for(i = 1; i < argc; i++)
	{
		if(!strcmp(argv[i], "-l")) options |= OPT_L;
		if(!strcmp(argv[i], "-w")) options |= OPT_W;
		if(!strcmp(argv[i], "-c")) options |= OPT_C;
	}

	if(!options) options = OPT_L | OPT_W | OPT_C;

	for(i = 1; i < argc; i++)
	{
		if(strcmp(argv[i], "-l") && strcmp(argv[i], "-w") && strcmp(argv[i], "-c"))
		{
			printInfo(argv[i], options);
			use_stdin = 0;
		}
	}

	if(use_stdin) printInfo(0, options);

	return 0;
}
